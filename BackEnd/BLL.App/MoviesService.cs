﻿using System;
using System.Collections.Generic;
using Contracts.BLL.App;
using DAL.App;
using Domain;

namespace BLL.App
{
    public class MoviesService : IMoviesService
    {
        MoviesRepository repository;
        
        public MoviesService()
        {
            repository = new MoviesRepository();
        }
        
        public ICollection<Movie> All()
        {
            return repository.All();
        }

        public Movie GetDetailsById(int id)
        {

            return repository.GetById(repository.All(), id);
        }
    }
}