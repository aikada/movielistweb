﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BLL.App;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMoviesService _moviesService; 
        
        public MoviesController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }
        // GET: api/Movies
        [HttpGet]
        public ICollection<Movie> GetMovies()
        {
            return _moviesService.All();
        }
        
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public Movie GetMovieDetails(int id)
        {
            return _moviesService.GetDetailsById(id);
        }
        
    }
}