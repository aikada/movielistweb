﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Movie : BaseEntity
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        
        public ICollection<MovieInCategory> MovieInCategories { get; set; }
        public ICollection<MovieInList> MovieInLists { get; set; }

    }
}