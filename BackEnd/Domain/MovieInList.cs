namespace Domain
{
    public class MovieInList
    {
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    
        public int MoviesListId { get; set; }
        public MoviesList MoviesList { get; set; }
    }
}