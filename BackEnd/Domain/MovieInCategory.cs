namespace Domain
{
    public class MovieInCategory : BaseEntity
    {
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}