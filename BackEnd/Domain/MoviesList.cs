using System.Collections.Generic;

namespace Domain
{
    public class MoviesList : BaseEntity
    {
        public ICollection<MovieInList> MovieInLists { get; set; }

    }
}