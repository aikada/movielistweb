﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using Contracts.DAL.App;
using Domain;

namespace DAL.App
{
    public class MoviesRepository : IMoviesRepository
    {
        public ICollection<Movie> All()
        {
            
            ICollection<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    Id = 0,
                    Title = "Scary Stories to Tell in the Dark",
                    Year = 2019,
                    Description = "The shadow of the Bellows family has loomed large " +
                                  "in the small town of Mill Valley for generations.",
                    Rating = 3,
                    MovieInCategories = new List<MovieInCategory>()
                    {
                        new MovieInCategory(){
                        MovieId = 0,
                        CategoryId = 0
                        },
                        new MovieInCategory(){
                        MovieId = 0,
                        CategoryId = 1
                        }
                    }
                },

                new Movie()
                {
                    Id = 1,
                    Title = "Pet Sematary",
                    Year = 2019,
                    Description = "Dr. Louis Creed and his wife, Rachel, relocate from " +
                                  "Boston to rural Maine with their two young children.",
                    Rating = 3,
                    MovieInCategories = new List<MovieInCategory>()
                    {
                        new MovieInCategory(){
                            MovieId = 0,
                            CategoryId = 0
                        }
                    }
                },

                new Movie()
                {
                    Id = 2,
                    Title = "Monty Python and the Holy Grail",
                    Year = 1975,
                    Description = "A comedic send-up of the grim circumstances of the " +
                                  "Middle Ages as told through the story of King Arthur " +
                                  "and framed by a modern-day murder investigation.",
                    Rating = 4,
                    MovieInCategories = new List<MovieInCategory>()
                    {
                        new MovieInCategory(){
                            MovieId = 0,
                            CategoryId = 1
                        }
                    }
                }
            };

            return movies;
        }
        
        public Movie GetById(ICollection<Movie> movies, int id)
        {
            var movie = All().FirstOrDefault(m => m.Id == id);
            
            return movie;
        }
    }
}