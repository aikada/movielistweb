﻿using System;
using System.Collections.Generic;
using Domain;

namespace Contracts.BLL.App
{
    public interface IMoviesService
    {
        ICollection<Movie> All();
        Movie GetDetailsById(int id);
        
    }
}