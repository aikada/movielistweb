﻿using System;
using System.Collections.Generic;
using Domain;

namespace Contracts.DAL.App
{
    public interface IMoviesRepository
    {
        ICollection<Movie> All();
        Movie GetById(ICollection<Movie> movies, int id);

    }
}