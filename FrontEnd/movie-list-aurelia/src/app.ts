import {log, AppConfig} from "./app-config";
import {IMovie} from "./interfaces/IMovie";
import {observable, inject, BindingEngine, ICollectionObserverSplice, autoinject} from "aurelia-framework";

@autoinject
export class App {

  private appConfig: AppConfig = new AppConfig();

  private categories: ({ id: number; name: string })[] = [
    {id: 0, name: 'Horror',},
    {id: 1, name: 'Comedy',}
  ];

  private selectedCategoryIds = [];

  private filteredMovies: IMovie[] = [];
  private movies: IMovie[] = [];
  
  private movieForDetailView: IMovie;
  

  @observable private search: string = '';

  constructor(private bindingEngine: BindingEngine) {
    let subscription = this.bindingEngine.collectionObserver(this.selectedCategoryIds)
      .subscribe(this.collectionChanged.bind(this));
  }
  
  attached() {
    log.debug('attached');
    this.fetchAll();
  }

  backToList() {
    this.movieForDetailView = null;
  }

  collectionChanged(splices: Array<ICollectionObserverSplice<string>>) {
    this.filterBySearch();
  }


  fetchAll() {
    log.debug('fetchAll');
    let url = this.appConfig.apiUrl + 'movies';
    
    fetch(url).then(r => r.json())
      .then(data => {
        log.debug(data);
        this.movies = data;
        this.filteredMovies = data;
      })
      .catch(e => console.log("No movies found"));
  }

  fetchDetails(id: number) {
    this.movieForDetailView = this.movies[id];
  }

  filterBySearch() {
    this.filteredMovies = this.movies;
    if (this.search !== '' || this.selectedCategoryIds.length > 0) {
      
      let filteredMovies: IMovie[] = [];
      for (const movie of this.movies) {
        if (this.movieHasCheckedCategories(movie) || this.selectedCategoryIds.length == 0) {
          if (movie.title.toUpperCase().includes(this.search.toUpperCase())) {
            filteredMovies.push(movie);
          }
        }
      }
      this.filteredMovies = filteredMovies;
    }
  }
  
  movieHasCheckedCategories(movie) {
    for (const selectedCategoryId of this.selectedCategoryIds) {
      for (const movieInCategory of movie.movieInCategories) {
        if (movieInCategory.categoryId == selectedCategoryId) {
          log.debug(movie.title);
          return true;
        }
      }
    }
    return false;
  }

  searchChanged(newValue, oldValue) {
    this.filterBySearch();
  }

}
