import {IMovie} from "./IMovie";
import {ICategory} from "./ICategory";

export interface IMovieInCategory{
  id: number;
  movieId: number;
  categoryId: number;
}
