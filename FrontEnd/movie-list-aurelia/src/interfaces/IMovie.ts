import {IMovieInCategory} from "./IMovieInCategory";

export interface IMovie {
  id: number;
  title: string;
  year: number;
  description: string;
  rating: number;
  
  movieInCategories?: IMovieInCategory[];
}
