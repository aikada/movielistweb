import {IMovieInCategory} from "./IMovieInCategory";

export interface ICategory {
  id: number;
  name: string;
  movieInCategories?: IMovieInCategory[];
}
