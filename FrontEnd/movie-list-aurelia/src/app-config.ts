import {LogManager, autoinject} from "aurelia-framework";

export var log = LogManager.getLogger('AppConfig');

@autoinject
export class AppConfig {
  
  public apiUrl = 'https://localhost:5001/api/';

  constructor() {
    log.debug('constructor');
  }

}
