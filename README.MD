## Backend
MovieListWeb solution

* Domain entities
* Repository and service class for retrieving all Movie data (hardcoded string) and single using movie id
* Movie API controllers for read-only (also linked in Home view)
* Allow CORS

## Frontend
##### movie-list-aurelia
~~~
au run
~~~
* Gets data from backend (must be running). API address is set to https://localhost:5001/
* Displays all movies in list with title, year, rating and category id (id names currently in hardcoded array)
* Details view
* Search/filter functionality. Refreshes displayed list after search parameters change.


##### movie-list-vue
~~~
npm run serve
~~~

* Gets data from backend (must be running). API address is set to https://localhost:5001/
* Displays all movies in list with title, year, rating and category id (id names currently in hardcoded array)
* Show/hide description
* Search/filter functionality not yet implemented
* Basic Foundation css styling

##### Default JSON API links:
https://localhost:5001/api/Movies/
https://localhost:5001/api/Movies/idd

If using different address replace in App.vue

Created by Airiin Kadakmaa
